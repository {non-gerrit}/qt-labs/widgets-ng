<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name></name>
    <message id="test_id">
        <source>Completely irrelevant source text</source>
        <translation>This is a test string.</translation>
    </message>
    <message id="untranslated_id">
        <source>This has no translation.</source>
    </message>
    <message id="this_another_id">
        <source>Foo bar.</source>
        <comment>Warn me!</comment>
    </message>
    <message>
        <source>Drop me!</source>
    </message>
</context>
</TS>
