#include <QtGui>

// Include header files for application components.
// ...

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Set up and show widgets.
    // ...

    return app.exec();
}
