/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at http://qt.nokia.com/contact.
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT_NO_DIRECTFB

#include "qdirectfbscreen.h"
#include "qdirectfbpaintdevice.h"
#include "qdirectfbpaintengine.h"

QDirectFBPaintDevice::QDirectFBPaintDevice(QDirectFBScreen *scr)
    : QCustomRasterPaintDevice(0), dfbSurface(0), lockedImage(0), screen(scr),
      bpl(-1), lockFlgs(DFBSurfaceLockFlags(0)), mem(0), engine(0)
{}

QDirectFBPaintDevice::~QDirectFBPaintDevice()
{
    delete lockedImage;
    delete engine;
}


IDirectFBSurface *QDirectFBPaintDevice::directFBSurface() const
{
    return dfbSurface;
}


void QDirectFBPaintDevice::lockDirectFB(DFBSurfaceLockFlags flags)
{
    if (!(lockFlgs & flags)) {
        if (lockFlgs)
            unlockDirectFB();
        mem = QDirectFBScreen::lockSurface(dfbSurface, flags, &bpl);
        Q_ASSERT(mem);
        const QSize s = size();
        lockedImage = new QImage(mem, s.width(), s.height(), bpl,
                                 QDirectFBScreen::getImageFormat(dfbSurface));
        lockFlgs = flags;
    }
}


void QDirectFBPaintDevice::unlockDirectFB()
{
    if (!lockedImage || !QDirectFBScreen::instance())
        return;

    dfbSurface->Unlock(dfbSurface);
    delete lockedImage;
    lockedImage = 0;
    mem = 0;
    lockFlgs = DFBSurfaceLockFlags(0);
}


void *QDirectFBPaintDevice::memory() const
{
    return mem;
}


QImage::Format QDirectFBPaintDevice::format() const
{
    return QDirectFBScreen::getImageFormat(dfbSurface);
}


int QDirectFBPaintDevice::bytesPerLine() const
{
    if (bpl == -1) {
        // Can only get the stride when we lock the surface
        Q_ASSERT(!lockedImage);
        QDirectFBPaintDevice* that = const_cast<QDirectFBPaintDevice*>(this);
        that->lockDirectFB(DSLF_READ);
        Q_ASSERT(bpl != -1);
    }
    return bpl;
}


QSize QDirectFBPaintDevice::size() const
{
    int w, h;
    dfbSurface->GetSize(dfbSurface, &w, &h);
    return QSize(w, h);
}

int QDirectFBPaintDevice::metric(QPaintDevice::PaintDeviceMetric metric) const
{
    if (!dfbSurface)
        return 0;

    switch (metric) {
    case QPaintDevice::PdmWidth:
    case QPaintDevice::PdmHeight:
        return (metric == PdmWidth ? size().width() : size().height());
    case QPaintDevice::PdmWidthMM:
        return (size().width() * 1000) / dotsPerMeterX();
    case QPaintDevice::PdmHeightMM:
        return (size().height() * 1000) / dotsPerMeterY();
    case QPaintDevice::PdmPhysicalDpiX:
    case QPaintDevice::PdmDpiX:
        return (dotsPerMeterX() * 254) / 10000; // 0.0254 meters-per-inch
    case QPaintDevice::PdmPhysicalDpiY:
    case QPaintDevice::PdmDpiY:
        return (dotsPerMeterY() * 254) / 10000; // 0.0254 meters-per-inch
    case QPaintDevice::PdmDepth:
        DFBSurfacePixelFormat format;
        dfbSurface->GetPixelFormat(dfbSurface, &format);
        return QDirectFBScreen::depth(format);
    case QPaintDevice::PdmNumColors: {
       if (lockedImage)
            return lockedImage->numColors();

        DFBResult result;
        IDirectFBPalette *palette = 0;
        unsigned int numColors = 0;

        result = dfbSurface->GetPalette(dfbSurface, &palette);
        if ((result != DFB_OK) || !palette)
            return 0;

        result = palette->GetSize(palette, &numColors);
        palette->Release(palette);
        if (result != DFB_OK)
            return 0;

        return numColors;
    }
    default:
        qCritical("QDirectFBPaintDevice::metric(): Unhandled metric!");
        return 0;
    }
}

QPaintEngine *QDirectFBPaintDevice::paintEngine() const
{
    return engine;
}

#endif

